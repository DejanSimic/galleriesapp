import Vue from "vue";
import VueRouter from "vue-router";

import GalleriesApp from "../components/GalleriesApp";
import CreateGallery from "../components/CreateGallery";
import GalleryShow from "../components/GalleryShow";
import MyGalleries from "../components/MyGalleries";
import UsersGalleries from "../components/UsersGalleries";
import Login from "../components/Login";
import Register from "../components/Register";
import { requiresAuth, guestOnly } from "./guards";

Vue.use(VueRouter);

export const router = new VueRouter({
  routes: [
    { path: "/", redirect: "/galleries" },
    { path: "*", redirect: "/galleries" },
    {
      path: "/galleries",
      component: GalleriesApp,
      name: "galleries"
    },
    {
      path: "/create",
      component: CreateGallery,
      name: "create-galery",
      meta: { requiresAuth: true }
    },
    {
      path: "/galleries/:id",
      component: GalleryShow,
      name: "show-galery"
    },
    {
      path: "/login",
      component: Login,
      name: "login",
      meta: { guestOnly: true }
    },
    {
      path: "/register",
      component: Register,
      name: "register",
      meta: { guestOnly: true }
    },
    {
      path: "/my-galleries",
      component: MyGalleries,
      name: "my-galleries",
      meta: { requiresAuth: true }
    },
    {
      path: "/authors/:id",
      component: UsersGalleries,
      name: "users-gallery"
    },
    {
      path: "/edit-gallery/:id",
      component: CreateGallery,
      name: "edit-gallery",
      meta: { requiresAuth: true }
    }
  ],
  mode: "history"
});
router.beforeEach((to, from, next) => {
  Promise.resolve(to)
    .then(requiresAuth)
    .then(guestOnly)
    .then(() => {
      next();
    })
    .catch(redirect => {
      next(redirect);
    });
});
