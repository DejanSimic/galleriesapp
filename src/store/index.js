import Vue from "vue";
import Vuex from "vuex";

import { galleryService } from "../service/GalleryService";
import { authService } from "../service/AuthService";

Vue.use(Vuex);

export const store = new Vuex.Store({
  state: {
    searchTerm: "",
    galleries: [],
    isAuthenticated: authService.isAuthenticated(),
    authUserId: ""
  },
  getters: {
    getSearchTerm(state) {
      return state.searchTerm;
    },
    getGalleries(state) {
      return state.galleries;
    },
    getIsAuthenticated(state) {
      return state.isAuthenticated;
    },
    getAuthUserId(state) {
      return state.authUserId;
    }
  },
  mutations: {
    setSearchTerm(state, searchTerm) {
      state.searchTerm = searchTerm;
    },
    setGalleries(state, galleries) {
      state.galleries = galleries;
    },
    setIsAuthenticated(state, auth) {
      state.isAuthenticated = auth;
    },
    setAuthUserId(state, authUserId) {
      state.authUserId = authUserId;
    }
  },
  actions: {
    fetchGalleries(store) {
      galleryService.getAll().then(response => {
        store.commit("setGalleries", response.data);
      });
    },
    fetchMyGalleries(store) {
      galleryService.getMyGalleries().then(response => {
        store.commit("setGalleries", response.data);
      });
    },
    fetchUsersGalleries(store, id) {
      galleryService.getUsersGalleries(id).then(response => {
        store.commit("setGalleries", response.data);
      });
    }
  }
});
