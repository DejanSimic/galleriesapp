import Axios from "axios";

export default class GalleryService {
  constructor() {
    Axios.defaults.baseURL = "http://localhost:8000/api/";
  }
  getAll() {
    return Axios.get("galleries");
  }
  getOne(id) {
    return Axios.get(`galleries/${id}`);
  }
  getMyGalleries() {
    return Axios.get("usersgalleries");
  }
  getUsersGalleries(id) {
    return Axios.get(`usersgalleries/${id}`);
  }
  edit(id, gallery) {
    return Axios.put(`galleries/${id}`, gallery);
  }
  store(gallery) {
    return Axios.post("galleries", gallery);
  }
  deleteGallery(id) {
    return Axios.delete(`galleries/${id}`);
  }
  addComment(comment, galleryId) {
    return Axios.post(`comment/${galleryId}`, comment);
  }
  deleteComment(id) {
    return Axios.delete(`comment/${id}`);
  }
}
export const galleryService = new GalleryService();
