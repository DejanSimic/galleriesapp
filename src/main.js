import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue/dist/bootstrap-vue.css";

import Vue from "vue";
import App from "./App.vue";
import BootstrapVue from "bootstrap-vue";
import VeeValidate from "vee-validate";
import VModal from "vue-js-modal";
import VuePaginate from "vue-paginate";

import { router } from "./router";
import { store } from "./store";

import { authService } from "./service/AuthService";

if (localStorage.getItem("loginToken")) {
  authService.setAxiosDefaultAuthorizationHeader();
}

Vue.config.productionTip = false;
Vue.use(BootstrapVue);
Vue.use(VeeValidate);
Vue.use(VModal, { dialog: true });
Vue.use(VuePaginate);

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
