import moment from "moment";

export const DateMixin = {
  filters: {
    formatDate(str, outputFormat = "HH:mm DD.MM.YYYY") {
      return moment(str).format(outputFormat);
    },

    diffForHumans(str) {
      return moment(str).from(moment());
    }
  }
};
